import React from "react";
import styles from "./Article.module.scss";

const Article = ({ title, intro }) => {
  return (
    <div className={styles.article}>
      <div className={styles.title}>{title}</div>
      <div className={styles.intro}>{intro}</div>
      <div className={styles.readMore}>read more</div>
    </div>
  );
};

export default Article;
