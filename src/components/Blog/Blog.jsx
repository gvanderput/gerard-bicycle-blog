import React, { lazy, Suspense } from "react";
import Article from "../Article/Article";
import styles from "./Blog.module.scss";
const LatestOffer = lazy(() => import("SHOP/LatestOffer"));

const Blog = () => {
  return (
    <div className={styles.blog}>
      <h1 className={styles.header}>Gerard's Blog</h1>
      <Article
        title="7 Tips for Becoming a Competent JavaScript Developer"
        intro="I've walked a long road to get where I'm at. For more than two decades, I've been writing code, working for small (...)"
      />
      <Suspense fallback={<span>Loading...</span>}>
        <LatestOffer />
      </Suspense>
      <Article
        title="My Everyday Struggles as a Senior Developer"
        intro="I've been working for one of the largest software companies in the world for more than a decade. With (...)"
      />
    </div>
  );
};

export default Blog;
