import React from "react";
import ReactDOM from "react-dom/client";
import "semantic-ui-css/semantic.min.css";
import Blog from "./components/Blog/Blog";

const container = document.querySelector("#container");
const root = ReactDOM.createRoot(container);
root.render(<Blog />);
